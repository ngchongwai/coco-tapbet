const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, (e: cc.Touch) => {
            this.getComponent(cc.Animation).play("slash1");
        })

        // todo delete mouse event
        // this.node.on(cc.Node.EventType.MOUSE_DOWN, (e: cc.Event.EventMouse) => {
        //     this.getComponent(cc.Animation).play("slash1");
        // })
    }

    start() {

    }

    update(dt) {

    }
}
