import HeroController from "./HeroController";
import heroTopToogle from "./UI Control/HeroTopToogleControl";
import state from "./Helper/state";
import Utils from "./Helper/Utils";

let monsterCharacter;
let monsterHp;
let hpbar;
let monsterReady = true;
let hitAnimation;

let monsterFadeIn;
let monsterFadeOut;
let sequenceMonster;

let balanceText;

let numberTexture;

cc.Class({
    extends: cc.Component,

    properties: {
        heroes: {
            default: [],
            type: cc.Node
        },
        hit: {
            default: null,
            type: cc.Prefab
        },
        numberTexture: {
            default: [],
            type: cc.SpriteFrame
        }
    },

    onLoad() {
        hitAnimation = this.hit;
        numberTexture = this.numberTexture;
        balanceText = cc.find("Canvas/UI/Top Menu Panel/Top Menu Button Center/Money")
        balanceText.getComponent(cc.Label).string = state.get('balance');
        monsterHp = 100;

        monsterCharacter = this.node;
        monsterCharacter.getComponent(sp.Skeleton).setCompleteListener(() => {
            monsterCharacter.getComponent(sp.Skeleton).setAnimation(0, Math.random() > 0.2 ? 'idle_1' : 'idle_2', false)
        });

        monsterFadeIn = cc.fadeIn(0.5);
        monsterFadeOut = cc.fadeOut(0.5);
        sequenceMonster = cc.sequence(monsterFadeOut, monsterFadeIn);
    },

    // @ts-ignore
    attackMonster() {
        for (let i = 0; i < this.heroes.length; i++) {
            HeroController.heroAttack(this.heroes[i]);
        }

        attackOnce()
    }
});

function skillDamage(skillNo) {
    return new Promise((resolve) => {
        let attackCount = 0;
        let attackArray = [];
        if (skillNo == 1) {
            attackCount = 10;
        } else if (skillNo == 2) {
            attackCount = 25;
        } else if (skillNo == 3) {
            attackCount = 50;
        }

        for (let i = 0; i < attackCount; i++) {
            attackArray.push(attackOnce)
        }

        runSerial(attackArray).then(() => {
            resolve();
        });
    })


    function runSerial(promises) {
        let result = Promise.resolve();
        promises.forEach(task => {
            result = result.then(() => task());
        });
        return result;
    }
}

function totalDamage() {
    let multiplier = 0;
    for (let i = 0; i < heroTopToogle.getSelectedArray().length; i++) {
        multiplier += heroTopToogle.getSelectedArray()[i];
    }
    state.set('totalDamage', state.get('equipment') + state.get('equipment') * multiplier);
    return state.get('totalDamage')
}


function hitMonster() {
    monsterCharacter = cc.find("Canvas/Monster");
    monsterCharacter.getComponent(sp.Skeleton).setAnimation(0, 'damage', false);
}

function hitStatusDisplay(hitType, options) {
    let node = cc.instantiate(hitAnimation);

    node.parent = cc.find("Canvas/Hit Status");

    node.setPosition(options.x, options.y);
    node.scaleX = options.scale * 2;
    node.scaleY = options.scale * 2;
    node.getComponent(cc.Animation).play(hitType);

    setTimeout(() => {
        node.destroy();
    }, 1000);
}

function attackOnce() {
    return new Promise((resolve) => {
        if (monsterReady) {
            hitMonster();

            totalDamage();

            //temp random
            let hitStatus;
            let hitMultiplier = 0;
            let randomType = Math.round(Math.random() * 20);
            if (randomType < 10) {
                hitStatus = 'Miss';
            } else if (randomType >= 10 && randomType < 15) {
                hitMultiplier = 1;
                hitStatus = 'BodyShot';
            } else if (randomType >= 15 && randomType < 18) {
                hitMultiplier = 2;
                hitStatus = 'Critical';
            } else if (randomType >= 18 && randomType < 20) {
                hitMultiplier = 3;
                hitStatus = 'HeadShot';
            }

            let scalingMultiplier = (0.2 * (hitMultiplier / 2));
            let options = {
                x: Math.floor(Math.random() * 400) - 200,
                y: Math.floor(Math.random() * 400) - 200,
                scale: 0.2 + scalingMultiplier,
                offsetX: 80 + (hitMultiplier * 20),
                offsetY: -80 + (1 / (0.1 + hitMultiplier) * 5),
            }
            hitStatusDisplay(hitStatus, options);

            let damageDeal = state.get('totalDamage') * hitMultiplier;

            state.set('balance', state.get('balance') - state.get('totalDamage') + damageDeal)
            balanceText.getComponent(cc.Label).string = state.get('balance');

            let winNumber = Utils.createSpriteNumbers(options, numberTexture, damageDeal.toString());

            winNumber.parent = cc.find("Canvas/Hit Status/Number");
            let numFadeIn = cc.fadeTo(0.2, 255);
            let numFadeOut = cc.fadeTo(0.2, 0);

            winNumber.opacity = 0;
            winNumber.runAction(numFadeIn);
            setTimeout(() => {
                winNumber.runAction(numFadeOut);
                setTimeout(() => {
                    winNumber.destroy();
                }, 200)
            }, 800)

            setHPBar(damageDeal).then((data) => {
                resolve(data);
            });
        }
    })
}

function setHPBar(damage) {
    return new Promise((resolve) => {
        hpbar = cc.find("Canvas/UI/Top Menu Panel/HP Bar");
        hpbar.getComponent(cc.ProgressBar).progress -= (parseFloat(damage) / monsterHp);

        if (hpbar.getComponent(cc.ProgressBar).progress <= 0) {
            monsterHp = monsterHp * 2;
            monsterReady = false;
            monsterCharacter.runAction(monsterFadeOut);
            setTimeout(() => {
                hpbar.getComponent(cc.ProgressBar).progress = 1;
                monsterCharacter.runAction(monsterFadeIn);
                setTimeout(() => {
                    monsterReady = true;
                    resolve('kill');
                }, 500)
            }, 500)
        } else {
            setTimeout(() => {
                resolve('alive');
            }, 50)
        }
    })
}

export default {
    skillDamage,
    hitMonster,
}