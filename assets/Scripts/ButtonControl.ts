cc.Class({
    extends: cc.Component,
    properties: {

        sprite: {
            default: null,
            type: cc.Node
        },
        textureDown: {
            default: null,
            type: cc.Texture2D
        },
        textureUp: {
            default: null,
            type: cc.Texture2D
        },
    },

    onLoad() {
        let sprite = this.sprite;
        let textureDown = this.textureDown;
        let textureUp = this.textureDown;
        this.node.on(cc.Node.EventType.MOUSE_DOWN, (e: cc.Event.EventMouse) => {
            if (textureDown)
                sprite.getComponent(cc.Button).normalSprite.setTexture(textureDown);
        })
        this.node.on(cc.Node.EventType.MOUSE_UP, (e: cc.Event.EventMouse) => {
            if (textureUp)
                sprite.getComponent(cc.Button).normalSprite.setTexture(textureUp);
        })
    }
});