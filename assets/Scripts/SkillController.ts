import moveParticle from "./moveParticle";
import SliderControl from "./UI Control/SliderControl";

let xInitPosition;
let yInitPosition;

let meteorDownAnimation;
let meteorAnimationWithEase;

let clickEventHandler;
let allSkillButton;
let attackButton;
let lowerTabButton;

cc.Class({
    extends: cc.Component,
    // properties:
    //     {
    //         // skill: {
    //         //     default: 0,
    //         //     type: cc.Integer
    //         // },
    //         button: cc.Button
    //     },

    onLoad() {
        allSkillButton = cc.find("Canvas/UI/Skill Slider/Handle");
        attackButton = cc.find("Canvas/Monster");
        lowerTabButton = cc.find("Canvas/UI/Bottom Menu Toggle Container");
    },

// @ts-ignore
    triggerSkill(event, customEventData) {
        moveParticle.triggerSkill(customEventData).then(() => {
            for (let i = 0; i< allSkillButton.childrenCount; i++){
                allSkillButton.children[i].getComponent(cc.Button).interactable = true;

            }
            attackButton.getComponent(cc.Button).interactable = true;
            lowerTabButton.getChildByName('Heroes').getComponent(cc.Button).interactable = true;
            lowerTabButton.getChildByName('Equipment').getComponent(cc.Button).interactable = true;
        });
        for (let i = 0; i< allSkillButton.childrenCount; i++){
            allSkillButton.children[i].getComponent(cc.Button).interactable = false;
        }
        attackButton.getComponent(cc.Button).interactable = false;
        lowerTabButton.getChildByName('Heroes').getComponent(cc.Button).interactable = false;
        lowerTabButton.getChildByName('Equipment').getComponent(cc.Button).interactable = false;
    },
});