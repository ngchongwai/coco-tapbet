import state from './Helper/state';
import MonsterController from "./AttackController";

let heroCharacter;
let heroList;

cc.Class({
    extends: cc.Component,

    onLoad() {
        heroCharacter = this.node;
        heroList = cc.find("Canvas/Heroes");

        heroCharacter.getComponent(sp.Skeleton).setCompleteListener(() => {
            for (let i = 1; i < heroList.children.length; i++) {
                heroList.children[i].getComponent(sp.Skeleton).setAnimation(0, 'idle_1', false);
            }
        });

        if (heroCharacter.children[0]) {
            heroCharacter.children[0].getComponent(cc.ParticleSystem).stopSystem();
        }
    },


});

function changeEquipment(equipmentNo) {
    state.set('idleAnimation', `idle_${equipmentNo}`);
    state.set('slashAnimation', `damage_${equipmentNo}`);
    heroList.children[0].getComponent(sp.Skeleton).setAnimation(0, state.get('idleAnimation'), true);
}

function heroAttack(hero = null) {
    if (hero.children[0]) {
        hero.children[0].getComponent(cc.ParticleSystem).resetSystem();
    }
    if (hero) {
        heroList.children[0].getComponent(sp.Skeleton).setAnimation(0, state.get('slashAnimation'), false);
        for (let i = 1; i < heroList.children.length; i++) {
            let randomAnimation = Math.random() > 0.5 ? 'damage_1' : 'damage_2';
            heroList.children[i].getComponent(sp.Skeleton).setAnimation(0, randomAnimation, false);
        }

        heroList.children[0].getComponent(sp.Skeleton).setAnimation(0, state.get('slashAnimation'), false);
    } else {
        heroCharacter.getComponent(sp.Skeleton).setAnimation(0, state.get('slashAnimation'), false);
    }


}

export default {
    heroAttack,
    changeEquipment,
}