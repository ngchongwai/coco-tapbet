import AttackController from "./AttackController";

let xInitPosition;
let yInitPosition;

let meteor;
let meteorDownAnimation;
let meteorAnimationWithEase;
let meteorReturnAnimation;
let sequenceMeteor;

let tornado;

let explosion;

cc.Class({
    extends: cc.Component,

    properties: {
        skillParticle1: {
            default: null,
            type: cc.Node
        },
        skillParticle2: {
            default: null,
            type: cc.Node
        },
        skillParticle3: {
            default: null,
            type: cc.Node
        }
    },

    onLoad() {
        meteor = this.skillParticle1;
        meteor.getComponent(cc.ParticleSystem).stopSystem();
        xInitPosition = this.node.x;
        yInitPosition = this.node.y;
        meteorDownAnimation = cc.moveTo(2, 0, 0);
        meteorAnimationWithEase = meteorDownAnimation.easing(cc.easeBackOut());
        meteorReturnAnimation = cc.moveTo(0, -500, 500);
        sequenceMeteor = cc.sequence(meteorDownAnimation, meteorReturnAnimation)

        tornado = this.skillParticle2;
        tornado.getComponent(cc.ParticleSystem).stopSystem();

        explosion = this.skillParticle3;
        explosion.getComponent(cc.ParticleSystem).stopSystem();
    },
});

function triggerSkill(skillNo) {
    return new Promise((resolve) => {
        if (skillNo == 1) {
            triggerMeteor();
        } else if (skillNo == 2) {
            triggerTornado();
        } else if (skillNo == 3) {
            triggerExplosion();
        }
        AttackController.skillDamage(skillNo).then(() => {
            resolve()

            meteor.getComponent(cc.ParticleSystem).stopSystem();
            tornado.getComponent(cc.ParticleSystem).stopSystem();
            explosion.getComponent(cc.ParticleSystem).stopSystem();
        });
    })

}

function triggerMeteor() {
    meteor.setPosition(-500, 500);
    meteor.runAction(meteorDownAnimation);
    meteor.getComponent(cc.ParticleSystem).resetSystem();
}

function triggerTornado() {
    tornado.getComponent(cc.ParticleSystem).resetSystem();
}

function triggerExplosion() {
    explosion.getComponent(cc.ParticleSystem).resetSystem();
}

export default {
    triggerSkill,
}