import heroTopToogle from './HeroTopToogleControl';
import HeroToogleControl from "./HeroToogleControl";

cc.Class({
    extends: cc.Component,
    properties: {
        buttonNo: {
            default: 0,
            type: cc.Integer
        }
    },

    onLoad() {
        let heroToggle = cc.find("Canvas/UI/Heroes Menu/Hero ScrollView/view/content");

        this.node.on(cc.Node.EventType.TOUCH_END, (e: cc.Touch) => {
            if (this.node.getComponent(cc.Toggle).interactable) {
                if (heroTopToogle.getSelectedArray().length >= this.buttonNo) {
                    heroToggle.getChildByName(`Hero ${heroTopToogle.getSelectedArray()[this.buttonNo - 1]} Tab`).getChildByName('Hero Button').getComponent(cc.Toggle).isChecked = false;
                    heroTopToogle.heroDisable(heroTopToogle.getSelectedArray()[this.buttonNo - 1])
                }

                HeroToogleControl.disableHeroSelectorChecker()
            }
        })
    }
})