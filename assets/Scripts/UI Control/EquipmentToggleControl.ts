import state from "./../helper/state";
import HeroController from "../HeroController";

cc.Class({
    extends: cc.Component,
    properties: {
        tabTexture: {
            default: null,
            type: cc.Texture2D
        },
        selectedTexture: {
            default: null,
            type: cc.Texture2D
        },
        selectedIcon: {
            default: null,
            type: cc.Texture2D
        },
        buttonNo: {
            default: 0,
            type: cc.Integer
        }
    },

    onLoad() {

        let equipment = cc.find("Canvas/UI/Equipment Menu/Equipment ScrollView/view/content/New ToggleContainer").getComponent(cc.ToggleContainer);
        let showItemIcon = cc.find("Canvas/UI/Equipment Menu/Selected Item/Selected Item Icon")
        let showItemText = cc.find("Canvas/UI/Equipment Menu/Selected Item/Selected Damage Text")

        this.node.on(cc.Node.EventType.TOUCH_START, () => {
            this.node.getChildByName('Background').opacity = 0;
            this.node.getChildByName('checkmark').opacity = 0;
        })

        this.node.on(cc.Node.EventType.TOUCH_END, (e: cc.Touch) => {
            this.node.getChildByName('Background').opacity = 255;
            this.node.getChildByName('checkmark').opacity = 255;

            for (let i = 0; i < equipment.node.childrenCount; i++) {
                equipment.node.children[i].getChildByName('Equipment Tab').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.tabTexture);
            }
            this.node.getChildByName('Equipment Tab').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.selectedTexture);
            showItemIcon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.selectedIcon);
            state.set('equipment', this.buttonNo);
            HeroController.changeEquipment(this.buttonNo);
            showItemText.getComponent(cc.RichText).string = this.node.getChildByName("Damage Discription").getComponent(cc.RichText).string
        })
    }
});