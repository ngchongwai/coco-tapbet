import SliderControl from "./SliderControl";
import state from "../Helper/state";

cc.Class({
    extends: cc.Component,
    // properties: {
    //     sprite: {
    //         default: null,
    //         type: cc.Node
    //     }
    // },

    onLoad() {

        // this.node.on(cc.Node.EventType.TOUCH_END, () => {
        //     this.sliderPressed();
        // })
    },

// @ts-ignore
    sliderPressed() {

        SliderControl.moveSliderDown();

        state.set('sliderToggleState', -1);

        let bottomMenuHeroes = cc.find("Canvas/UI/Bottom Menu Toggle Container/Heroes");
        let bottomMenuEquipment = cc.find("Canvas/UI/Bottom Menu Toggle Container/Equipment");
        bottomMenuHeroes.getChildByName('checkmark').active = false;
        bottomMenuEquipment.getChildByName('checkmark').active = false;
        this.node.active = false;
    },
})