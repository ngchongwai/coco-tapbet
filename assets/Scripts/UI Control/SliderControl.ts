import state from "../Helper/state";

let sliderUp = -1;
let moveUpTime = 0.5;
let moveDownTime = 0.25;

cc.Class({
    extends: cc.Component,
    // properties: {
    //     ToggleButton: {
    //         default: [],
    //         type: cc.Button
    //     }
    // },

    onLoad() {

        // this.node.on(cc.Node.EventType.TOUCH_START, () => {
        //     this.node.opacity = 0;
        // })
        //
        // this.node.on(cc.Node.EventType.TOUCH_END, () => {
        //     this.node.opacity = 255;
        // })

    },

    // @ts-ignore
    buttonPressed(listener, i) {
        let bottomMenuHeroes = cc.find("Canvas/UI/Bottom Menu Toggle Container/Heroes");
        let bottomMenuEquipment = cc.find("Canvas/UI/Bottom Menu Toggle Container/Equipment");
        let bottomMenu = [bottomMenuHeroes, bottomMenuEquipment];

        for (let j = 0; j < bottomMenu.length; j++) {
            bottomMenu[j].getComponent(cc.Button).interactable = false;
            // bottomMenu[j].off(cc.Node.EventType.TOUCH_START);
            // bottomMenu[j].off(cc.Node.EventType.TOUCH_END);
        }

        if (state.get('sliderToggleState') == -1) {

            bottomMenu[i].getChildByName('checkmark').active = true;
            state.set('sliderToggleState', i);
            ;
        } else if (state.get('sliderToggleState') == i) {
            bottomMenu[i].getChildByName('checkmark').active = false;
            state.set('sliderToggleState', -1);
        } else {
            bottomMenu[state.get('sliderToggleState')].getChildByName('checkmark').active = false;
            bottomMenu[i].getChildByName('checkmark').active = true;
            state.set('sliderToggleState', i);
        }

        this.sliderPressed(i).then(() => {

            for (let j = 0; j < bottomMenu.length; j++) {
                bottomMenu[j].getComponent(cc.Button).interactable = true;
            }
        });
    },

// @ts-ignore
    sliderPressed(buttonNo) {
        return new Promise((resolve) => {

            let ease = cc.moveTo(moveUpTime, 0, -150);
            let moveUp = ease.easing(cc.easeBackOut());

            let heroMenu = cc.find("Canvas/UI/Heroes Menu");
            let equipmentMenu = cc.find("Canvas/UI/Equipment Menu");
            let allMenu = [heroMenu, equipmentMenu];
            let anyButtonPressed = false;

            moveSliderDown();

            cc.find("Canvas/UI/Bottom Menu Toggle Container/Closed Bottom Menu Toggle").active = false;
            // make slider move up or not
            setTimeout(() => {
                if (state.get('sliderToggleState') > -1) {
                    allMenu[state.get('sliderToggleState')].runAction(moveUp);
                    sliderUp = state.get('sliderToggleState');
                }

                cc.find("Canvas/UI/Bottom Menu Toggle Container/Closed Bottom Menu Toggle").active = true;
            }, sliderUp > -1 ? (moveDownTime * 1000) : 0);

            if (anyButtonPressed) {
                setTimeout(() => {
                    resolve();
                }, (state.get('sliderToggleState') > -1 ? (moveUpTime + moveDownTime) : moveDownTime) * 1000)
            } else {
                setTimeout(() => {
                    resolve();
                }, (moveUpTime) * 1000)
            }
        })
    },
})

function moveSliderDown() {
    let heroMenu = cc.find("Canvas/UI/Heroes Menu");
    let equipmentMenu = cc.find("Canvas/UI/Equipment Menu");
    let allMenu = [heroMenu, equipmentMenu];
    let moveDown = cc.moveTo(moveDownTime, 0, -1200);

    if (sliderUp > -1) {
        allMenu[sliderUp].runAction(moveDown);
        sliderUp = -1;
    }
}

export default {
    moveSliderDown,
}