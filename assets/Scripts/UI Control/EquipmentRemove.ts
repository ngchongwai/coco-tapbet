cc.Class({
    extends: cc.Component,
    properties: {
        defaultTexture: {
            default: null,
            type: cc.Texture2D
        },
    },

    onLoad() {

        let equipment = cc.find("Canvas/UI/Equipment Menu/Equipment ScrollView/view/content/New ToggleContainer").getComponent(cc.ToggleContainer);
        let showItemIcon = cc.find("Canvas/UI/Equipment Menu/Selected Item/Selected Item Icon")
        let showItemBackground = cc.find("Canvas/UI/Equipment Menu/Selected Item/Selected Item Background")
        let showItemButton = cc.find("Canvas/UI/Equipment Menu/Selected Item/Remove Equipment")

        this.node.on(cc.Node.EventType.TOUCH_END, () => {

            for (let i = 0; i < equipment.toggleItems.length; i++) {
                equipment.toggleItems[i].isChecked = false
            }
            showItemIcon.active = false;
            showItemBackground.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(this.defaultTexture);
            showItemButton.active = false;
        })
    }
});