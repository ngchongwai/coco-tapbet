import heroTopToogle from './HeroTopToogleControl';


let heroToggle;
let disableTabTexture;
let enableTabTexture;

cc.Class({
    extends: cc.Component,
    properties: {
        enableTexture: {
            default: null,
            type: cc.Texture2D
        },
        disableTexture: {
            default: null,
            type: cc.Texture2D
        },
        targetButtonInteractable: {
            default: false,
        },
        buttonNo: {
            default: 0,
            type: cc.Integer
        }
    },

    onLoad() {
        heroToggle = cc.find("Canvas/UI/Heroes Menu/Hero ScrollView/view/content");
        disableTabTexture = this.disableTexture;
        enableTabTexture = this.enableTexture;

        this.node.on(cc.Node.EventType.TOUCH_START, () => {
            this.node.opacity = 0;
        })

        this.node.on(cc.Node.EventType.TOUCH_END, (e: cc.Touch) => {
            this.node.opacity = 255;

            if (this.node.getComponent(cc.Toggle).interactable) {
                setTimeout(() => {

                    if (this.node.getComponent(cc.Toggle).isChecked) {
                        heroTopToogle.heroEnable(this.buttonNo);
                    }
                    else {
                        heroTopToogle.heroDisable(this.buttonNo);
                    }

                    disableHeroSelectorChecker();

                }, 10)
            }
        })
    }
});

function disableHeroSelectorChecker(){

    for (let i = 0; i < heroToggle.childrenCount; i++) {
        heroToggle.children[i].getChildByName('Hero Button').getComponent(cc.Toggle).interactable = true;

        heroToggle.children[i].getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(enableTabTexture);
    }

    if (heroTopToogle.getSelectedArray().length >= 4) {
        for (let i = 0; i < heroToggle.childrenCount; i++) {
            heroToggle.children[i].getChildByName('Hero Button').getComponent(cc.Toggle).interactable = false

            heroToggle.children[i].getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(disableTabTexture);
        }

        for (let i = 0; i < heroTopToogle.getSelectedArray().length; i++) {
            heroToggle.children[heroTopToogle.getSelectedArray()[i] - 1].getChildByName('Hero Button').getComponent(cc.Toggle).interactable = true;

            heroToggle.children[heroTopToogle.getSelectedArray()[i] - 1].getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(enableTabTexture);
        }
    }


    for (let i = 1; i < cc.find(`Canvas/Heroes`).childrenCount; i++) {
        cc.find(`Canvas/Heroes/SubHero${i}`).active = false;
    }

    for (let i = 0; i < heroTopToogle.getSelectedArray().length; i++) {
        cc.find(`Canvas/Heroes/SubHero${heroTopToogle.getSelectedArray()[i]}`).active = true;
    }
}

export default {
    disableHeroSelectorChecker,
}