let selectedArray = [];
let subHeroArrayTexture;

cc.Class({
    extends: cc.Component,
    properties: {
        hero1Texture: {
            default: null,
            type: cc.Texture2D
        },
        hero2Texture: {
            default: null,
            type: cc.Texture2D
        },
        hero3Texture: {
            default: null,
            type: cc.Texture2D
        },
        hero4Texture: {
            default: null,
            type: cc.Texture2D
        },
        hero5Texture: {
            default: null,
            type: cc.Texture2D
        },
        hero6Texture: {
            default: null,
            type: cc.Texture2D
        },
    },

    onLoad() {
        subHeroArrayTexture = [this.hero1Texture, this.hero2Texture, this.hero3Texture, this.hero4Texture, this.hero5Texture, this.hero6Texture]
    },
});

function heroEnable(buttonNo) {
    selectedArray.push(buttonNo);
    selectedArray.sort(
        function (a, b) {
            return a - b
        });
    checkTopButton();
}

function heroDisable(buttonNo) {
    for (let i = 0; i < selectedArray.length; i++) {
        if (selectedArray[i] === buttonNo) {
            selectedArray.splice(i, 1)
            selectedArray.sort(
                function (a, b) {
                    return a - b
                });
        }
    }
    checkTopButton();
}

function getSelectedArray() {
    return selectedArray
}

function checkTopButton() {
    let heroTopToggle = cc.find("Canvas/UI/Heroes Menu/Sprite Hero Group");

    for (let i = 0; i < heroTopToggle.childrenCount; i++) {
        heroTopToggle.children[i].getComponent(cc.Toggle).isChecked = false;
        heroTopToggle.children[i].getComponent(cc.Toggle).interactable = false;
    }

    for (let i = 0; i < selectedArray.length; i++) {
        heroTopToggle.children[i].getComponent(cc.Toggle).isChecked = true;
        heroTopToggle.children[i].getComponent(cc.Toggle).interactable = true;
        heroTopToggle.children[i].getChildByName('checkmark').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(subHeroArrayTexture[selectedArray[i]-1])
    }
}

export default {
    heroEnable,
    heroDisable,
    getSelectedArray,
}

// export default {
// }