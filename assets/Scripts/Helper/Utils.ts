function serial(tasks) {
    return tasks.reduce((promise, task) => promise.then(previous => runPromise(task, previous)), Promise.resolve(null))
}

function runPromise(task, previous) {
    return task();
}

function createSpriteNumbers(options, numberTextures, string) {
    if (!string) {
        throw new Error('createSpritesText requires a string');
    }
    if (!numberTextures) {
        throw new Error('createSpritesText requires the textures for the string');
    }

    let numberContainer = new cc.Node('Sprite');
    let charactersArray = string.split('');

    for (let i = 0; i < charactersArray.length; i++) {
        let currentTexture = numberTextures[charactersArray[i]];
        // let currentSprite = cc.instantiate(currentTexture);
        let currentSprite = new cc.Node('Sprite');
        let sp = currentSprite.addComponent(cc.Sprite);

        sp.spriteFrame = currentTexture;

        currentSprite.x = options.x + options.offsetX * (i) * options.scale - (options.offsetX / 2 * options.scale);
        currentSprite.y = options.y + options.offsetY;
        currentSprite.scaleX = options.scale;
        currentSprite.scaleY = options.scale;
        numberContainer.addChild(currentSprite);
    }

    return numberContainer;
}

export default {
    createSpriteNumbers,
    serial,
    runPromise,
}