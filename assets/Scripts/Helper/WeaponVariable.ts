import state from "./state";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    // @property(cc.Label)
    // label: cc.Label = null;
    //
    // @property
    // text: string = 'hello';

    onLoad () {
        state.set('equipment', 1)
        state.set('idleAnimation', `idle_1`);
        state.set('slashAnimation', `damage_1`);

        state.set('sliderReady', true);
        state.set('sliderToggleState', -1);

        state.set('balance', 1000000);
    }

    // start () {
    //
    // }

    // update (dt) {}
}
