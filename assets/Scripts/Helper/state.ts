let state = {};

function get(key) {
    // todo: returning default as undefined will cause something break.
    // we should support returning default value if state not found.
    return state[key];
}

function set(key, value) {
    state[key] = value;
}

export default {
    get,
    set,
};
